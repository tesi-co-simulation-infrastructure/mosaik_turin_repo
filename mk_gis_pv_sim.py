import mosaik_api
import geopandas as gpd
import collections
import pandas as pd
import gis_pv_sim.simulator as gispvsim
import time
# import ray

META = {
    'models': {
        'PVSystem': {
            'public': True,
            'params': ['num_sec', 'GisAreaPath', 'FIDS', 'PVSspec'],
            'attrs': ['power_dc', 'n_mp', 'T_ext', 'zenith', 'k_b', 'k_d'],
        },
    },
}

# @ray.remote


class GISPVSIM(mosaik_api.Simulator):
    def __init__(self):
        super().__init__(META)
        self.simulator = gispvsim.GISPVsimulator()
        self.eid_prefix = 'PV_'
        self.entities = {}  # Maps EIDs to model indices in self.simulator
        self.GisAreaPath = None

    def init(self, sid, start_date, num_sec, eid_prefix=None, GisAreaPath=None):
        self.start_date = pd.to_datetime(start_date).tz_localize('UTC')
        # Why?
        # self.simulator.GisArea = GisArea
        self.GisAreaPath = GisAreaPath
        self.num_sec = num_sec
        if eid_prefix is not None:
            self.eid_prefix = eid_prefix
        return self.meta

    def create(self, num, model, FIDS=[0], PVSspec=[0]):
        next_eid = len(self.entities)
        entities = []
        # Gis Area are needed
        gdf_PVs, gdf_section = gispvsim.read_gis_data(num_sec=self.num_sec)
        for i in range(next_eid, next_eid + num):
            start_time = time.time()
            eid = '%s%d' % (self.eid_prefix, FIDS[i])
            AreaPV = {
                'FID': FIDS[i], 'Area': gdf_PVs.loc[FIDS[i], 'Area_reale']}
            if len(PVSspec) > 1:
                self.simulator.add_PV(AreaPV, PVSystemSpec=PVSspec[i])
                self.entities[eid] = i
                entities.append({'eid': eid, 'type': model})
            else:
                self.simulator.add_PV(AreaPV, PVSystemSpec=PVSspec[0])
                self.entities[eid] = i
                entities.append({'eid': eid, 'type': model})
            end_time = time.time()
            print(f'Created {i} in {int(end_time-start_time)}')
        # self.simulator.drop_not_used()
        self.simulator.read_beam_diff()
        return entities

    def step(self, time, inputs):
        # TODO finire comando step
        sim_time = self.start_date + pd.Timedelta(time, unit='s')
        data = {}
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                data[attr] = sum(values.values())
        # Perform simulation step
        data['time'] = sim_time
        self.simulator.step(data)
        return time + 15 * 60  # Step size is 15 minute

    def get_data(self, outputs):
        models = self.simulator.models
       # print()
       # print(models)
        data = {}
        for eid, attrs in outputs.items():
            model_idx = self.entities[eid]
            data[eid] = {}
            # print(len(models),model_idx)
            for attr in attrs:
                if attr not in self.meta['models']['PVSystem']['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)
                data[eid][attr] = getattr(models[model_idx], attr)
        return data

# @ray.remote


def main():
    return mosaik_api.start_simulation(GISPVSIM())


if __name__ == '__main__':
    main()
