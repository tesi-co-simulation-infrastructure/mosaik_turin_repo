import numpy as np
from math import *
import Configuration
import random
import os
from memory_profiler import profile


path = os.path.dirname(__file__) + "/data/loads"


class Device(object):
    #@profile
    def __init__(self):
        self.id_device=0
        self.StartTime=[]
        self.EndTime=[]
        self.status=0 #on=1 off=0 #standby=2
        self.res= Configuration.appliances_res
        self.interval= Configuration.interval
        self.Profile=0
        self.type=None
        self.LoadTrace=None
        self.LoadTrace2=None
        self.res= Configuration.appliances_res

    def resize(self):
        if self.res>1:
            self.LoadTrace=self.LoadTrace.reshape(-1,self.res).mean(axis=1)
            if self.LoadTrace2 is not None:
                self.LoadTrace2=self.LoadTrace2.reshape(-1,self.res).mean(axis=1)
    
    def simulate(self,timestamp,houseBehaviour):

            #turn on
        if houseBehaviour>0 and self.status==0:
            self.Profile=self.LoadTrace
            self.status=1
            self.StartTime=np.append(self.StartTime,timestamp)
        #is on write consumption
        elif houseBehaviour>0 and self.status==1:
            if self.LoadTrace2 is not None:
                self.Profile=self.LoadTrace2
            else:
                self.Profile=self.LoadTrace
        #turn off
        if houseBehaviour==0 and self.status==1:
                self.status=0
                self.EndTime=np.append(self.EndTime,timestamp)
        #is of write 0
        if houseBehaviour==0 and self.status==0:
            self.Profile=0


    def add_0(self):
        self.status=0
        self.Profile=0


class Hoven(Device):
    def __init__(self,data):
        super(Hoven, self).__init__()
        self.number=data['#w_cycle']
        self.type='Hoven'
        self.LoadTrace=np.loadtxt(os.path.join(path,'Hoven1.csv'),dtype='float16')
        self.LoadTrace2=np.loadtxt(os.path.join(path,'Hoven2.csv'),dtype='float16')
        self.resize()

class Television(Device):
    def __init__(self):
        super(Television, self).__init__()
        self.type='TV'
        self.LoadTrace=np.loadtxt(os.path.join(path,'TV1.csv'),dtype='float16')
        self.LoadTrace2=np.loadtxt(os.path.join(path,'TV2.csv'),dtype='float16')
        self.resize()
            
class Desktop(Device):
    def __init__(self):
        super(Desktop, self).__init__()
        self.type='Desktop'
        self.LoadTrace=np.full(self.interval,200,dtype='float16')


class Laptop(Device):
    def __init__(self):
        super(Laptop, self).__init__()
        self.type='Laptop'
        self.LoadTrace=np.full(self.interval,50,dtype='float16')


class Radio(Device):
    def __init__(self):
        super(Radio, self).__init__()
        self.type='Radio'
        self.LoadTrace=np.full(self.interval,50,dtype='float16')


class Iron(Device):
    def __init__(self):
        super(Iron, self).__init__()
        self.type='Iron'
        self.LoadTrace=np.loadtxt(os.path.join(path,'Iron1.csv'),dtype='float16')
        self.LoadTrace2=np.loadtxt(os.path.join(path,'Iron2.csv'),dtype='float16')
        self.resize()

class VacuumCleaner(Device):
    def __init__(self):
        super(VacuumCleaner, self).__init__()
        self.LoadTrace=np.full(self.interval,400,dtype='float16')
        self.type='VacuumCleaner'


class Microwave(Device):
    def __init__(self):
        super(Microwave, self).__init__()
        self.type='Microwave'
        self.LoadTrace=np.full(self.interval,700,dtype='float16')
                

class Elecooker(Device):
    def __init__(self):
        super(Elecooker, self).__init__()
        self.type='Cookers'
        self.LoadTrace=np.loadtxt(os.path.join(path,'Elecook1.csv'),dtype='float16')
        self.LoadTrace2=np.loadtxt(os.path.join(path,'Elecook2.csv'),dtype='float16')
        self.resize()

class Lights(Device):
    def __init__(self,room):
        super(Lights, self).__init__()
        self.type='Light'
        self.place=room
        self.LoadTrace=np.full(self.interval,50,dtype='float16')
        self.deviation=float(np.random.uniform(-1,1,1)*10)
    
    def simulate(self,timestamp,houseBehaviour):

        if Configuration.getRadiation(timestamp)<Configuration.rad_limit+self.deviation:
            #turn on
            if houseBehaviour>0 and self.status==0:

                self.Profile=self.LoadTrace
                self.status=1
                self.StartTime=np.append(self.StartTime,timestamp)
            #is on write consumption
            elif houseBehaviour>0 and self.status==1:

                self.Profile=self.LoadTrace
            #turn off
            if houseBehaviour==0 and self.status==1:
                    self.status=0
                    self.EndTime=np.append(self.EndTime,timestamp)
            #is of write 0
            if houseBehaviour==0 and self.status==0:
                self.Profile=0
        else:
            self.Profile=0

class Cycle_Device(object):
    #@profile
    def resize(self):
        if self.res>1:
            self.LoadTrace=self.LoadTrace.reshape(-1,self.res).mean(axis=1)
            if self.LoadTrace2 is not None:
                self.LoadTrace2=self.LoadTrace2.reshape(-1,self.res).mean(axis=1)
    
    
    def __init__(self):
        self.id_device=0
        self.StartTime=[]
        self.EndTime=[]
        self.status=0 #on=1 off=0 standby=2
        self.interval= Configuration.interval
        self.Profile=0
        self.LoadTrace=np.array([],dtype='float16')
        self.LoadTrace2=None
        self.count=0
        self.res= Configuration.appliances_res

    
    def simulate(self,timestamp,houseBehaviour):
        
        if houseBehaviour>0 and self.status==0:
            self.Profile=self.LoadTrace[:self.interval]
            self.status=1
            self.StartTime=np.append(self.StartTime,timestamp)
            self.count+=1
    
        elif self.status==1:
            if len(self.LoadTrace)>=self.count*self.interval+1:
                self.Profile=self.LoadTrace[self.count*self.interval:(1+self.count)*self.interval]
                self.count+=1
            else:
                self.time=self.count
                self.status=0
                self.count=0
                self.EndTime=np.append(self.EndTime,timestamp)
                self.Profile=0
        elif self.status==0:
            self.Profile=0

class Washingmachine(Cycle_Device):
    def __init__(self,data):
        super(Washingmachine, self).__init__()
        self.type='Washingmachine'
        self.size=data['size']
        self.number=data['#w_cycle']
        self.number_we=data['#wend_cycle']
        self.LoadTrace=np.loadtxt(os.path.join(path,'Washmachine.csv'),dtype='float16')
        self.resize()  
        self.lenTrace=len(self.LoadTrace)/self.interval
         
    #def select_type(self):
        #TODO

class Dishwasher(Cycle_Device):
    def __init__(self,data):
        super(Dishwasher, self).__init__()
        self.type='Dishwasher'
        self.number=data['#w_cycle']
        self.number_we=data['#wend_cycle']
        self.LoadTrace=np.loadtxt(os.path.join(path,'Dishwasher.csv'),dtype='float16')
        self.resize()  
        self.lenTrace=len(self.LoadTrace)/self.interval
        
    #def select_type(self):
        #TODO

class Fridge(Cycle_Device):
    def __init__(self,data):
        super(Fridge, self).__init__()
        self.size=data['size']
        self.type='Fridge'
        self.LoadTrace=np.loadtxt(os.path.join(path,'Fridge.csv'),dtype='float16')
        first=np.random.randint(len(self.LoadTrace)-1)
        self.LoadTrace=np.append(self.LoadTrace[first:],self.LoadTrace[:first])
        self.resize()    
        
    def simulate(self,timestamp,houseBehaviour):
        if self.status==0:
            self.Profile=self.LoadTrace[:self.interval]
            self.status+=1
            self.StartTime=np.append(self.StartTime,timestamp)
            self.count+=1
        #is on write consumption
        elif self.status>=1:
            if len(self.LoadTrace)>=self.count*self.interval+self.count:
                self.Profile=self.LoadTrace[self.count*self.interval:(1+self.count)*self.interval]
                self.count+=1
                self.status+=1
            else:
                self.Profile=self.LoadTrace[:self.interval]
                self.count=1
                self.status+=1
    #def select_type(self):
        #TODO

class Dryer(Cycle_Device):
    def __init__(self):
        super(Dryer, self).__init__()
        self.type='Dryer'
        self.LoadTrace=np.loadtxt(os.path.join(path,'Dryer.csv'),dtype='float16')
        self.resize()
        self.lenTrace=len(self.LoadTrace)/self.interval
        
    #def select_type(self):
        #TODO
    
class WaterBoiler(object):
    #@profile
    def __init__(self, data,persons):
        self.setBoilerParameter(data)
        self.ison=dict.fromkeys(persons,False)
        self.LoadTrace=np.array([],dtype=np.float16)
        self.Profile=0
        self.id_device=0
        self.StartTime=[]
        self.EndTime=[]
        self.type='WaterBoiler'
        self.shower_flow=0.07 #l/s
        self.dish_flow=0.04 #l/s
        self.UA = 0.750/1000          #[kJ/(sec C)]
        self.diamter=0.55        #[m]
        self.lm3=1000.0       #[l/m^3]
        self.h=self.tankVol/(np.pi * (self.diamter/2)**2)/self.lm3 #[m]
        self.rho=1000.0 #[kg/m^3]
        self.Cp=4.1867 #[kJ/Kg/k]
        self.actual_voltage=240 #[V]
        self.nominal_voltage=240 #[V]
        self.setponit=55 #C
        self.deadband=4.0 #C
        self.Ton=self.setponit - self.deadband/2
        self.Toff=self.setponit + self.deadband/2
        self.T_in=10 #C
        self.flow = 0 # l/s
        self.heat_need=False
        self.Tamb=20 #[C]
        self.Cw=self.tankVol/self.lm3*self.rho*self.Cp #[kJ/C]
        self.Tw=np.random.uniform(self.Ton,self.Toff) #[C]
        self.model='ONENODE'
        self.TankStatus='FULL'
        self.state='STABLE'
        self.Tprecision = 0.01 #[C]
        self.h_precision=0.01  #[m]
        self.h0=self.h
        self.Tlow=self.T_in
        self.updateT_h(0,0)
        self.stay=0
        self.doneWash=False
        self.doneShower=False
        self.interval=600
        self.res= Configuration.appliances_res
        self.shower_dur=5*60

    def setBoilerParameter(self,data):

        if data['size']==1:
            self.tankVol=random.randrange(30,50,10) #[l]
            self.Power=1.0 #[kJ/sec] rated power of water heater
        elif data['size']==2:
            self.tankVol=random.randrange(50,70,10) #[l]
            self.Power=1.2 #[kJ/sec] rated power of water heater
        else:
            self.tankVol=random.randrange(80,120,10) #[l]
            self.Power=1.5       #[kJ/sec] rated power of water heater
    
    def getTempOneNodeModel(self,t1,t0,T0):
        mdot = self.Cp *  self.flow * self.rho / self.lm3
        b = -(self.UA + mdot) / self.Cw
        a = (self.actualP() + mdot*self.T_in+self.UA*self.Tamb)/self.Cw
        T1 = (exp(b*(t1-t0))*(a+b*T0)-a)/b
        return T1

    def getTimeOneNodeModel(self,T1,T0,t0):
        mdot = self.Cp*self.flow*self.rho/self.lm3
        b = -(self.UA + mdot) / self.Cw
        a = (self.actualP() + mdot*self.T_in+self.UA*self.Tamb)/self.Cw
        t1 =t0 + 1/b*(log(fabs(a+b*T1))-log(fabs(a+b*T0)))
        return t1

    def getHeightTWONODEModel(self,t1,t0):
        mdot = self.flow*self.rho/self.lm3*self.Cp
        a = (self.actualP() + self.UA*(self.Tamb-self.Tlow))/(self.Cw*(self.Tw-self.Tlow))*self.h - mdot/self.Cw*self.h
        b = -(self.UA/self.Cw)
        h1 = ((exp(b*(t1-t0))*(a+b*self.h0))-a)/b
        if h1<0:
            h1=0
            self.state='EMPTY'
        return h1

    def getTimeTWONODEModel(self,h1,h0,t0):
        mdot = self.flow*self.rho/self.lm3*self.Cp
        a = (self.actualP() + self.UA*(self.Tamb-self.Tlow))/(self.Cw*(self.Tw-self.Tlow))*self.h - mdot/self.Cw*self.h
        b = -(self.UA/self.Cw)
        dh1dt=fabs(self.getdHdt(h1))
        dh0dt=fabs(self.getdHdt(h0))
        t1 = t0 + (log(dh1dt)-log(dh0dt))/b
        return t1

    def getdHdt(self,h,t=0):
        if self.Tw - self.Tlow < 0.00001:
            return 0.0
        else:
            mdot = self.flow*self.rho/self.lm3*self.Cp
            a = (self.actualP() + self.UA*(self.Tamb-self.Tlow))/(self.Cw*(self.Tw-self.Tlow))*self.h - mdot/self.Cw*self.h
            b = -(self.UA/self.Cw)

            return a+b*h
    
    def getdTdt(self,T0,t=0):
        mdot = self.Cp *  self.flow * self.rho / self.lm3
        b = -(self.UA + mdot)/ self.Cw
        a = (self.actualP() + mdot*self.T_in+self.UA*self.Tamb)/self.Cw

        return a+b*T0

    def actualP(self):
        if self.heat_need == True:
            power=self.Power*(self.actual_voltage*self.actual_voltage)/ (self.nominal_voltage*self.nominal_voltage)
        else:
            power=0
        return power

    def termostat(self):
        self.tankstate()    
        if self.TankStatus == 'FULL':
            if (self.Tw - self.Tprecision < self.Ton):
                self.heat_need = True
            elif (self.Tw + self.Tprecision > self.Toff):
                self.heat_need = False
        elif self.TankStatus == 'PARTIAL' or self.TankStatus == 'EMPTY':
            self.heat_need = True

    def tankstate(self):
        if self.h0 >= self.h-self.h_precision:
            self.TankStatus = 'FULL'
        elif self.h0 <= self.h_precision:
            self.TankStatus = 'EMPTY'
        else:
            self.TankStatus = 'PARTIAL'

    def setModelState(self):
        if self.model == 'TWONODE':
            if self.h0==self.h and self.getdHdt(self.h0)>=0:
                self.model = 'ONENODE'
                self.state='STABLE'
            elif self.flow == 0:
                self.state = 'RECOVER'
            else:
                self.state = 'DISCHARGE'
        
        elif self.model == 'ONENODE':
            if self.flow > 0 and self.TankStatus!='EMPTY':
                self.model = 'TWONODE'
                self.state = 'DISCHARGE'
            elif self.flow>0:
                self.state = 'DISCHARGE'
            elif self.heat_need == True:
                self.state = 'RECOVER'
            elif self.heat_need == False:
                self.state = 'STABLE'

        elif self.TankStatus == 'EMPTY':
            self.model ='ONENODE'
            if self.flow > 0:
                self.state = 'DISCHARGE'
            else:
                self.state = 'RECOVER'


    def updateT_h(self,t1,t0):
        if self.model == 'ONENODE':
            self.Tw=self.getTempOneNodeModel(t1,t0,self.Tw)
        elif self.model == 'TWONODE':
            self.h0 = self.getHeightTWONODEModel(t1,t0)
            
            if self.h0 > self.h: 
            
                vol_over = self.tankVol/self.lm3 * (self.h0 - self.h ) / self.h
                energy_over = vol_over * self.rho  * self.Cp * (self.Tw - self.Tlow)
                self.Tw =self.Tw + energy_over/self.Cw
                self.h0 = self.h

            elif self.h0 < 0.000001:
                vol_over = self.tankVol/self.lm3 * self.h0/self.h
                energy_over = vol_over * self.rho * self.Cp * (self.Tw - self.Tlow)
                Tnew = self.Tlow + energy_over/self.Cw
                self.Tw = self.Tlow = Tnew;
                self.h0 = 0;
                self.TankStatus = 'EMPTY'
                self.model ='ONENODE'
                self.state = 'RECOVER'
        self.setModelState()
        self.tankstate()
        self.termostat()

    def getTimeforsimulate(self,flow,dur):
        self.flow=flow
        self.termostat()
        self.setModelState()
        if self.model == 'ONENODE':
            if self.state=='STABLE':
                self.wait = round(self.getTimeOneNodeModel(self.Ton,self.Tw,0)+0.5)
            elif self.state == 'RECOVER':
                self.wait = round(self.getTimeOneNodeModel(self.Toff,self.Tw,0)+0.5)
        elif self.model == 'TWONODE':
            if self.state== 'RECOVER':
                self.wait = round(self.getTimeTWONODEModel(self.h,self.h0,0)+0.5)
        

    

    def simulate(self,ACS):
        if ACS==1:
            self.getTimeforsimulate(self.shower_flow,self.shower_dur)
            self.updateT_h(self.shower_dur,0)
            self.LoadTrace=np.append(self.LoadTrace,np.ones(self.shower_dur)*self.actualP()*1000)
            self.getTimeforsimulate(0,0)
            self.stay=self.wait
            self.doneShower=False
            if self.stay<=(self.interval-self.shower_dur):
                self.updateT_h(self.stay,0)
                self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.stay))*self.actualP()*1000)
                self.getTimeforsimulate(0,0)
                self.updateT_h(self.interval-self.shower_dur-self.stay,0)
                self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.interval-self.shower_dur-self.stay))*self.actualP()*1000)
            else:
                self.updateT_h(self.interval-self.shower_dur,0)
                self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.interval-self.shower_dur))*self.actualP()*1000)
        elif ACS==2:
            self.getTimeforsimulate(self.dish_flow,self.interval)
            self.updateT_h(self.interval,0)
            self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.interval))*self.actualP()*1000)
            self.doneWash = False################

        else:
            self.getTimeforsimulate(0,0)
            self.stay=self.wait
            if 0<self.stay<self.interval: 
                self.updateT_h(self.stay,0)
                self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.stay))*self.actualP()*1000)
                self.getTimeforsimulate(0,0)
                self.updateT_h(self.interval-self.stay,0)
                self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.interval-self.stay))*self.actualP()*1000)
            elif self.stay<0:
                self.updateT_h(0,0)
                self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.interval))*self.actualP()*1000)
            else:
                self.updateT_h(self.interval,0)
                self.LoadTrace=np.append(self.LoadTrace,np.ones(int(self.interval))*self.actualP()*1000)