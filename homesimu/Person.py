import Configuration
import numpy as np
import transitions as tran
import semi_markov as sm
from collections import defaultdict
import pandas as pd
import os

path= os.path.dirname(__file__) + "/data"

dact=pd.read_csv(path + '/catpri.csv', dtype=str, index_col=0,header=None,squeeze=1).to_dict()
dact1=pd.read_csv(path + '/catcon.csv', dtype=str, index_col=0,header=None,squeeze=1).to_dict()

class Person(object):
    def __init__(self):
        self.week=sm.NHSMM()
        self.sat=sm.NHSMM()
        self.sun=sm.NHSMM()
        #self.chain={}
        self.person_id=0
        self.wait=0     
        self.chain=defaultdict(int)
        for act in np.unique(list(dact.values())):
            self.chain[act]=0
        self.chain['Presence']=1
        self.chain['Adult']=0
        self.chain['Active']=0
        self.chain['Shower']=0

        self.had_shower=False
        self.is_shower=False
        self.count=0

    def add_state_step(self,time,act):

        act=act.split('_')
        act1=dact1[act[1]]
        act=dact[act[0]]
        self.wait=int(self.wait)
        if act=='Absent':
            self.chain['Presence']=0
            self.chain[act]=1
        else:
            self.chain['Presence']=1
            self.chain[act]=1
            if act1!='00':
                self.chain[act1]=1
        if act!='Absent' and act!='Sleep':
            self.chain['Active']=1
            if self.adult==True:
                self.chain['Adult']=1
        if act=='Bathroom' and self.had_shower==False and self.wait>=3:
            self.chain['Shower']=1
            self.had_shower==True
            self.is_shower=True
        

    def simulate_step(self,time):

        #time=days[0]
        if self.count==0:
            self.day=time.day

        if time.day!=self.day:
            self.day=time.day
            self.had_shower=False
            self.is_shower=False

        if time.dayofweek==5 and time.time().hour==0 and time.time().minute==0:
            self.sat.state=self.week.state

        if time.dayofweek==6 and time.time().hour==0 and time.time().minute==0:
            self.sun.state=self.sat.state

        if time.dayofweek==0 and time.time().hour==0 and time.time().minute==0:
            self.week.state=self.sun.state

        if self.wait == 0:
            if time.dayofweek<5:
                if self.count is 0:
                    self.week.start(time.time())
                else:
                    self.week.next(time.time())

                self.add_state_step(time,self.week.state)
                self.wait=self.week.dur
                
            elif time.dayofweek==5:
                if len(self.chain) is 0:
                    self.sat.start(time.time())
                else:
                    self.sat.next(time.time())
                self.add_state_step(time,self.sat.state)
                self.wait=self.sat.dur
                
            elif time.dayofweek==6:
                if len(self.chain) is 0:
                    self.sun.start(time.time())
                else:
                    self.sun.next(time.time())
                self.add_state_step(time,self.sun.state)
                self.wait=self.sat.dur
        else:
            if time.dayofweek<5:
                self.wait=self.wait-1

            if time.dayofweek==5:
                self.wait=self.wait-1
                
            if time.dayofweek==6:
                self.wait=self.wait-1

        self.count+=1

class Infant(Person):
    def __init__(self,pid):
        super(Infant, self).__init__()
        self.week,self.sat,self.sun=tran.get_infant()
        self.person_id=pid
        self.adult=False
        self.type='Infant'

class Kid(Person):
    def __init__(self,pid):
        super(Kid, self).__init__()
        self.week,self.sat,self.sun=tran.get_kids()
        self.person_id=pid
        self.adult=False
        self.type='Kid'

class Stud(Person):
    def __init__(self,pid):
        super(Stud, self).__init__()
        self.week,self.sat,self.sun=tran.get_stud()
        self.person_id=pid
        self.adult=False
        self.type='Stud'

class Hwife(Person):
    def __init__(self,pid):
        super(Hwife, self).__init__()
        self.week,self.sat,self.sun=tran.get_hwife()        
        self.person_id=pid
        self.adult=True
        self.type='Hwife'
            
class WorkM(Person):
    def __init__(self,pid):
        super(WorkM, self).__init__()
        self.week,self.sat,self.sun=tran.get_workm()       
        self.person_id=pid
        self.adult=True
        self.type='WorkM'

class WorkF(Person):
    def __init__(self,pid):
        super(WorkF, self).__init__()
        self.week,self.sat,self.sun=tran.get_workf()
        self.person_id=pid
        self.adult=True
        self.type='WorkF'

class NoWorkM(Person):

    def __init__(self,pid):
        super(NoWorkM, self).__init__()
        self.week,self.sat,self.sun=tran.get_noworkm()    
        self.person_id=pid
        self.adult=True
        self.type='NoWorkM'

class NoWorkF(Person):
    def __init__(self,pid):
        super(NoWorkF, self).__init__()
        self.week,self.sat,self.sun=tran.get_noworkf()
        self.person_id=pid
        self.adult=True
        self.type='NoWorkF'

class OldM(Person):
    def __init__(self,pid):
        super(OldM, self).__init__()
        self.week,self.sat,self.sun=tran.get_oldm()
        self.person_id=pid
        self.adult=True
        self.type='OldF'

class OldF(Person):
    def __init__(self,pid):
        super(OldF, self).__init__()
        self.week,self.sat,self.sun=tran.get_oldf()
        self.person_id=pid
        self.adult=True
        self.type='OldM'


class FullM(Person):
    def __init__(self,pid):
        super(FullM, self).__init__()
        self.week,self.sat,self.sun=tran.get_fullm()       
        self.person_id=pid
        self.adult=True
        self.type='FullM'

class FullF(Person):
    def __init__(self,pid):
        super(FullF, self).__init__()
        self.week,self.sat,self.sun=tran.get_fullf()       
        self.person_id=pid
        self.adult=True
        self.type='FullF'

class PartialM(Person):
    def __init__(self,pid):
        super(PartialM, self).__init__()
        self.week,self.sat,self.sun=tran.get_Partialm()       
        self.person_id=pid
        self.adult=True
        self.type='PartialM'

class PartialF(Person):
    def __init__(self,pid):
        super(PartialF, self).__init__()
        self.week,self.sat,self.sun=tran.get_Partialf()       
        self.person_id=pid
        self.adult=True
        self.type='PartialF'

        
