import numpy as np
from math import *
from . import Configuration
import random
import os
from memory_profiler import profile
path = os.path.dirname(__file__) + "/data/loads"
res= Configuration.appliances_res
interval=Configuration.interval
class Loads(object):
	"""docstring for Loads"""
	def __init__(self):
		super(Loads, self).__init__()
		self.hoven1=np.loadtxt(os.path.join(path,'Hoven1.csv'),dtype='float16').reshape(-1,res).mean(axis=1)
		self.hoven2=np.loadtxt(os.path.join(path,'Hoven2.csv'),dtype='float16').reshape(-1,res).mean(axis=1)

		self.tv1=np.loadtxt(os.path.join(path,'TV1.csv'),dtype='float16').reshape(-1,res).mean(axis=1)
		self.tv2=np.loadtxt(os.path.join(path,'TV2.csv'),dtype='float16').reshape(-1,res).mean(axis=1)

		self.iron1=np.loadtxt(os.path.join(path,'Iron1.csv'),dtype='float16').reshape(-1,res).mean(axis=1)
		self.iron2=np.loadtxt(os.path.join(path,'Iron2.csv'),dtype='float16').reshape(-1,res).mean(axis=1)

		self.elec1=np.loadtxt(os.path.join(path,'Elecook1.csv'),dtype='float16').reshape(-1,res).mean(axis=1)
		self.elec2=np.loadtxt(os.path.join(path,'Elecook2.csv'),dtype='float16').reshape(-1,res).mean(axis=1)

		self.washingmachine=np.loadtxt(os.path.join(path,'Washmachine.csv'),dtype='float16').reshape(-1,res).mean(axis=1)

		self.dishwasher=np.loadtxt(os.path.join(path,'Dishwasher.csv'),dtype='float16').reshape(-1,res).mean(axis=1)

		self.fridge=np.loadtxt(os.path.join(path,'Fridge.csv'),dtype='float16').reshape(-1,res).mean(axis=1)
		self.dryer=np.loadtxt(os.path.join(path,'Dryer.csv'),dtype='float16').reshape(-1,res).mean(axis=1)

		self.desktop=np.full(interval,200,dtype='float16')
		self.laptop=np.full(interval,50,dtype='float16')
		self.radio=np.full(interval,50,dtype='float16')
		self.vacuum=np.full(interval,400,dtype='float16')
		self.micro=np.full(interval,700,dtype='float16')
		self.ligth=np.full(interval,50,dtype='float16')
