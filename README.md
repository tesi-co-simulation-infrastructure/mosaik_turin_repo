# Installation #
## On Linux
Install the libspatialindex via apt
```bash
apt update && apt install libspatialindex-dev
```
## On other SO
No additional dependency is needed

---
# Python dependencies
Run 
```bash
pip install -r requirements.txt
```
to install all the other dependencies

