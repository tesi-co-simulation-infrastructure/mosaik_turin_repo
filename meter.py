import collections
import pprint
import pickle
import numpy as np
import pandas as pd
import logging
import time

from pathlib import Path
import json

import mosaik_api

Path("./logs").mkdir(parents=True, exist_ok=True)
logger = logging.getLogger('meter')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s %(levelname)s] %(message)s')
file_handler = logging.FileHandler(f'./logs/meter_{int(time.time())}.log')
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)


META = {
    'models': {
        'MeterSystem': {
            'public': True,
            'params': ['ids', 'start_date', 'duration', 'num_sec'],
            'attrs': ['Load', 'Prod', 'Pnet'],
        },
    },
}


class SMModel:

    def __init__(self, name):
        self.name = name

        self.Pnet = 0
        self.Pload = 0
        self.Pprod = 0
        self.Enet = 0
        self.Eload = 0
        self.Eprod = 0
        self.Autocon = 0
        self.Autosuff = 0
        self.Immessa = 0


class MeterSim(mosaik_api.Simulator):

    def __init__(self):
        super().__init__(META)
       # self.simulator = MeterSystem.Simulator()
        self.eid_prefix = 'Cens_'
        self.eid = None
        self.entities = {}
        self.duration = None
        self.num_sec = None
        self.collect_data = False

    def init(self, sid, start_date, collect_data=False, output_name=None, sim_meta=None, eid_prefix=None):
      #  self.step_size = step_size
        # TODO: il collettore è da rivedere ma non è fondamentale (sostituisce il
        self.collect_data = collect_data

        if collect_data:
            #           self.data = nested_dict(lambda:collections.defaultdict(list))
            self.data = collections.defaultdict(
                lambda: collections.defaultdict(lambda: collections.defaultdict(list)))
            if output_name == None:
                self.output_name = 'test'
            else:
                self.output_name = output_name

        if self.meta['models'] == {}:
            self.meta['models'] = sim_meta['models']

        if eid_prefix is not None:
            self.eid_prefix = eid_prefix
        self.start_date = pd.to_datetime(start_date).tz_localize('UTC')
        return self.meta

    def create(self, num, model, duration=None, num_sec=None, ids=None):
        self.duration = duration
        self.num_sec = num_sec
        next_eid = len(self.entities)
        entities = []

        for i in range(next_eid, next_eid + num):
            eid = '%s%d' % (self.eid_prefix, ids[i])
            self.entities[eid] = SMModel(eid)
            entities.append({'eid': eid, 'type': model})

        return entities

    def step(self, time, inputs):
        for dest_eid, attrs in inputs.items():
            meter = self.entities[dest_eid]
            for attr, values in attrs.items():
                # System reference from the consumers (load is positive)
                if attr == 'Load':
                    Pload = sum(values.values())
                elif attr == 'Prod':
                    Pprod = sum(values.values())

            meter.Pload = Pload
            meter.Pprod = Pprod
            meter.Pnet = Pload - Pprod

            meter.Eload = meter.Eload + Pload/6
            meter.Eprod = meter.Eprod + Pprod/6
            meter.Enet = meter.Eload - meter.Eprod

            if meter.Pprod >= meter.Pload:
                meter.Autocon = meter.Autocon + meter.Pload/6
                meter.Immessa = meter.Immessa + (meter.Pprod - meter.Pload)/6

            else:
                meter.Autocon = meter.Autocon + meter.Pprod/6

            meter.Autosuff = meter.Autocon/meter.Eload * 100

            if self.collect_data:
                self.data[f'{meter.name}']['Pload'][time].append(int(Pload))
                self.data[f'{meter.name}']['Pprod'][time].append(int(Pprod))
                self.data[f'{meter.name}']['Pnet'][time].append(
                    int(meter.Pnet))

        return time + 10*60

    def get_data(self, outputs):
        data = {}
        for eid, attrs in outputs.items():
            meter = self.entities[eid]
            model_type = eid.split('_')[0]
            data[eid] = {}
            for attr in attrs:
                if attr not in self.meta['models'][model_type]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)
                data[eid][attr] = getattr(meter, attr)

        return data

    def finalize(self):
        measures = {}
        for eid in self.entities.keys():
            model = self.entities[eid]
            measures[eid] = {'Eprod': model.Eprod, 'Eload': model.Eload, 'Autocon': model.Autocon,
                             'Immessa': model.Immessa, 'Autosuff': model.Autosuff, 'Enet': model.Enet}

        Path("./OutputSimResults").mkdir(parents=True, exist_ok=True)
        with open(f'./OutputSimResults/output_values_%d_days_%d_sez.json' % (self.duration, self.num_sec), 'w') as outfile:
            json.dump(measures, outfile)

        if self.collect_data:
            Path("./OutputSimResults").mkdir(parents=True, exist_ok=True)
            with open('./OutputSimResults/output_profiles_%d_days_%d_sez.json' % (self.duration, self.num_sec), 'w') as outfile:
                json.dump(self.data, outfile)


def main():
    return mosaik_api.start_simulation(MeterSim())


if __name__ == '__main__':
    main()
