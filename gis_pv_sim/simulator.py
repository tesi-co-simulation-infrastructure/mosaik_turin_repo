from os import getpid
from functools import partial
import pandas as pd
import numpy as np
from math import *
import geopandas as gpd
import os
import time
import multiprocessing
import logging
from pathlib import Path
import time
# import ray
# path = os.path.dirname(',')

Path("./logs").mkdir(parents=True, exist_ok=True)
logger = logging.getLogger('gis_pv_sim')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s %(levelname)s] %(message)s')
file_handler = logging.FileHandler(f'./logs/gis_pv_sim_{int(time.time())}.log')
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

######################## CONSTANTS ########################

######################## DEFAULT PATH FOR SOURCES ########################
path = os.path.dirname(__file__)
BEAM_PATH = os.path.join(path, "beam_clear.zip")
DIFF_PATH = os.path.join(path, "diff_clear.zip")
AREA_PATH = os.path.join(path, "area_turin/areasuit.shp")
SECTION_PATH = 'sez_cens/torino_sezcens.shp'

######################## DEFAULT PV SPECS ########################
DEFAULT_PV_SPECS = {
    'Tc_noct': 45,
    'T_ex_noct': 20,
    'a_p': 0.0038,
    'ta': 0.9,
    'P_module': 283,
    'G_noct': 800,
    'G_stc': 1000,
    'ModuleArea': 1.725,
    'Tc_stc': 25.0,
}


def read_csv(path, FIDS):
    df = pd.read_csv(path, index_col='FID')
    df = df.loc[FIDS]
    df_without_mean_count = df.drop(columns=['mean', 'count'])
    return df_without_mean_count


def read_gis_data(num_sec, area_path=AREA_PATH):
    # Photovoltaic from DSM
    gdf_PVs_tot = gpd.read_file(area_path, crs='4326')
    gdf_PVs_tot = gdf_PVs_tot.set_index(gdf_PVs_tot['FID'])
    gdf_PVs_tot.crs = 'epsg:4326'
    # Census Sections from ISTAT
    gdf_section_tot = gpd.read_file(SECTION_PATH, crs='32632')
    gdf_section_tot = gdf_section_tot.to_crs(epsg='4326')
    # Connect PVs with selected Section
    gdf_section = gdf_section_tot.iloc[:num_sec]
    gdf_PVs = gpd.sjoin(
        gdf_PVs_tot, gdf_section[['SEZ2011', 'geometry']], op='intersects')
    gdf_PVs = gdf_PVs.sort_values(by=['SEZ2011'])
    return gdf_PVs, gdf_section


class PVSystem:
    """docstring for PVmodel"""

    def __init__(self, AreaPV={'FID': 0, 'Area': 0}, PVSystemSpec=DEFAULT_PV_SPECS):
        self.Tc_stc = PVSystemSpec['Tc_stc']+273.15
        self.Tc_noct = PVSystemSpec['Tc_noct']+273.15
        self.T_ex_noct = PVSystemSpec['T_ex_noct']+273.15
        self.a_p = PVSystemSpec['a_p']
        self.ta = PVSystemSpec['ta']
        self.P_module = PVSystemSpec['P_module']
        self.G_noct = PVSystemSpec['G_noct']
        self.G_stc = PVSystemSpec['G_stc']
        self.ModuleArea = PVSystemSpec['ModuleArea']
        self.n_mp_stc = self.P_module/self.ModuleArea/self.G_stc
        self.ModuleNumber = (int)(AreaPV['Area']/self.ModuleArea)
        self.P_sytem = self.ModuleNumber * self.P_module
        self.PVSurface = self.ModuleNumber * self.ModuleArea
        self.gti = 0
        self.power_dc = 0
        self.n_mp = 0
        self.FID = AreaPV['FID']

    def step(self):
        self.T_ext = 0
        T_sol = self.T_ext+273.15+0.05*self.gti
        Tc = T_sol+(self.Tc_noct-self.T_ex_noct)*(self.gti/self.G_noct)*(1-(self.n_mp_stc*(1-self.a_p*self.Tc_stc) /
                                                                            self.ta))/(1+(self.Tc_noct-self.T_ex_noct)*(self.gti/self.G_noct)*(self.a_p*self.n_mp_stc/self.ta))
        self.n_mp = self.n_mp_stc*(1-self.a_p*(Tc-self.Tc_stc))
        self.power_dc = self.PVSurface*self.gti*self.n_mp
        # print(self.power_dc)


class GISPVsimulator(object):

    def __init__(self):
        self.time = 0
        self.FIDS = []
        self.models = []
        self.data = []
        # self.GisArea = None # is this useful?
        # self.beam = read_csv(BEAM_PATH)
        # self.diff = read_csv(DIFF_PATH)
        # self.beam = self.beam.set_index(['FID'])
        # self.diff = self.diff.set_index(['FID'])
        # self.env_data = {}

    def add_PV(self, AreaPV={'FID': 0, 'Area': 0}, PVSystemSpec=DEFAULT_PV_SPECS):
        """Create an instances of ``Model`` with *init_val*."""
        model = PVSystem(AreaPV, PVSystemSpec)
        self.FIDS.append(model.FID)
        self.models.append(model)
        self.data.append([])  # Add list for simulation data

    def drop_not_used(self):
        # self.GisArea = self.GisArea.loc[self.FIDS]
        print(f'before beam mem: {self.beam.memory_usage().sum() / (1024**2)}')
        self.beam = self.beam.loc[self.FIDS]
        print(f'after beam mem: {self.beam.memory_usage().sum() / (1024**2)}')
        print(f'before diff mem: {self.diff.memory_usage().sum() / (1024**2)}')
        self.diff = self.diff.loc[self.FIDS]
        print(f'after beam mem: {self.diff.memory_usage().sum() / (1024**2)}')

    def read_beam_diff(self):
        self.beam = read_csv(BEAM_PATH, self.FIDS)
        self.diff = read_csv(DIFF_PATH, self.FIDS)
        logger.info(
            f'Beam mem usage {self.beam.memory_usage().sum() / (1024**2)} Mb')
        logger.info(
            f'Diff mem usage {self.diff.memory_usage().sum() / (1024**2)} Mb')

    def step(self, env_data=None):
        # env_data arriva dal meteo con un Pandas che ha zenith k_b k_d time
        # arrivano k_b e k_d dal meteo sim che fa dec.py
        month = {1: '17', 2: '47', 3: '75', 4: '105', 5: '135', 6: '162',
                 7: '198', 8: '228', 9: '258', 10: '288', 11: '318', 12: '344'}
        hour = {4: '04', 5: '05', 6: '06', 7: '07', 8: '08', 9: '09', 10: '10', 11: '11', 12: '12',
                13: '13', 14: '14', 15: '15', 16: '16', 17: '17', 18: '18', 19: '19', 20: '20', 21: '21', 22: '22'}
        minute = {0: '00', 15: '25', 30: '00', 45: '75'}
        # self.env_data = env_data
        if env_data['zenith'] < 85:
            index = env_data['time']
            start_time = time.time()
            beam_calc = self.beam[month[index.month]+'_'+hour[index.hour] +
                                  '_'+minute[index.minute]]*env_data['k_b']
            logger.debug(f'Beam access time {time.time() - start_time}')
            diff_calc = self.diff[month[index.month]+'_'+hour[index.hour] +
                                  '_'+minute[index.minute]]*env_data['k_d']
            radiation = beam_calc+diff_calc

            radiation = radiation.fillna(0)
            start_time = time.time()
            for i, model in enumerate(self.models):
                try:
                    gti = radiation.loc[model.FID].mean()
                except:
                    gti = radiation.loc[model.FID]

                model.gti = gti
                model.T_ext = env_data['T_ext']
                # print('PID',getpid())

                model.step()
                # print(model.gti)
            logger.debug(f'Iteration time on step: {time.time() - start_time}')
        else:
            for i, model in enumerate(self.models):
                model.gti = 0
                model.T_ext = env_data['T_ext']
                model.step()


'''
if __name__ == '__main__':
	gdf = gpd.read_file('area_turin/areasuit.shp',crs='4326')
	gdf = gdf.set_index(gdf['FID'])
	model = GISPVsimulator()
	model.GisArea = gdf
	model.add_PV(AreaPV = {'FID':0,'Area':0},PVSystemSpec={'Tc_noct':45,'T_ex_noct':20,'a_p':0.0038,'ta':0.9,'P_module':283,
				'G_noct':800,'G_stc':1000,'ModuleArea':1.725,'Tc_stc':25.0})

'''

if __name__ == '__main__':
    start_time = time.time()

    gdf = gpd.read_file('area_turin/areasuit.shp', crs='4326')
    gdf = gdf.set_index(gdf['FID'])
    model = GISPVsimulator()
    model.GisArea = gdf
    for i in gdf.index[:10000]:
        model.add_PV(AreaPV={'FID': i, 'Area': 10}, PVSystemSpec={'Tc_noct': 45, 'T_ex_noct': 20, 'a_p': 0.0038, 'ta': 0.9, 'P_module': 283,
                                                                  'G_noct': 800, 'G_stc': 1000, 'ModuleArea': 1.725, 'Tc_stc': 25.0})

    meteo = pd.read_csv('meteo_mosaik.csv',
                        index_col='Date_time', parse_dates=[0, ])
    data = {}
    print('inizio_sim')

    for i in range(40, 44):
        data['time'] = meteo.index[i]
        data['zenith'] = meteo.iloc[i]['zenith']
        data['T_ext'] = meteo.iloc[i]['T_ext']
        data['k_b'] = meteo.iloc[i]['k_b']
        data['k_d'] = meteo.iloc[i]['k_d']

        model.step(data)
        # print(i)
        # print(data['time'])
        for i in model.models:
            print(i.power_dc, i.FID, data['time'])

    print(time.time() - start_time)
