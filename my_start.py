# how to call from terminal: python my_start.py NUM_SEC time(days)

import mosaik
import mosaik.util
import geopandas as gpd
import os
import pandas as pd
import numpy as np
import pickle
import itertools
import time
from pathlib import Path
import json
import logging

Path("./logs").mkdir(parents=True, exist_ok=True)
logger = logging.getLogger('simulation_world')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s %(levelname)s] %(message)s')
file_handler = logging.FileHandler(f'./logs/sim_world_{int(time.time())}.log')
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)
logger.addHandler(file_handler)

SIM_CONFIG = {
    'HouseholdSim': {
        # 'python': 'homesim_mosaik:HouseholdSim',
        'cmd': 'python homesim_mosaik.py %(addr)s',
    },
    'GISPVSIM': {
        'python': 'mk_gis_pv_sim:GISPVSIM',
        # 'cmd': 'python mk_gis_pv_sim.py %(addr)s',  # 'host:5555',
    },
    "csv": {
        "python": "mk_csvsim:CSV"
    },

    'MeterSim': {
        # 'python': 'meter:MeterSim',
        'cmd': 'python meter.py %(addr)s',
    },
}

MK_CONFIG = {
    'addr': ('127.0.0.1', 5554),
    'start_timeout': 100,  # seconds default 10
    'stop_timeout': 100,  # seconds default 10
}

END = 10  # DEFINE THE NUMBER OF DAYS TO SIMULATE
END = END * 10 * 60 * 144
START_DATE = '2015-09-22 00:00:00'  # YYYY/MM/dd
# PVs Specifications
PVSystemsSpec = [{'Tc_noct': 45, 'T_ex_noct': 20, 'a_p': 0.0038, 'ta': 0.9,
                  'P_module': 283, 'G_noct': 800, 'G_stc': 1000, 'ModuleArea': 1.725, 'Tc_stc': 25.0, }]
# Meteo from WheatherUnderground
path = os.path.dirname('.')
METEO_FILE = 'meteo_mosaik.csv'
METEO_PATH = os.path.join(path, METEO_FILE)
# Restrict to a defined number of zones
NUM_SEC = 10  # DEFINE THE NUMBER OF SECTIONS TO SIMULATE


def read_gdf_data():
    # Photovoltaic from DSM
    gdf_PVs_tot = gpd.read_file(
        'gis_pv_sim/area_turin/areasuit.shp', crs='4326')
    gdf_PVs_tot = gdf_PVs_tot.set_index(gdf_PVs_tot['FID'])
    gdf_PVs_tot.crs = 'epsg:4326'
    # Census Sections from ISTAT
    gdf_section_tot = gpd.read_file('sez_cens/torino_sezcens.shp', crs='32632')
    gdf_section_tot = gdf_section_tot.to_crs(epsg='4326')
    # Connect PVs with selected Section
    gdf_section = gdf_section_tot.iloc[:NUM_SEC]
    gdf_PVs = gpd.sjoin(
        gdf_PVs_tot, gdf_section[['SEZ2011', 'geometry']], op='intersects')
    gdf_PVs = gdf_PVs.sort_values(by=['SEZ2011'])
    return gdf_PVs, gdf_section


def read_house_data(gdf_section):
    # Houses from populationsim
    houses_tot = pd.read_csv('pop_sim/synthetic_households.zip',
                             index_col='household_id')
    houses = houses_tot.loc[houses_tot['SEZ2011'].isin(gdf_section['SEZ2011'])]
    return houses.sort_values(by=['SEZ2011'])


def read_family_data(houses):
    # Families from populationsim
    fname = 'pop_sim/persons.pkl'
    with open(fname, 'rb') as f:
        families_tot = pickle.load(f)
    families = {key: families_tot[key] for key in houses.index.astype(str)}
    return families


def connect_meters(world, pv_models, home_models, meter_models):
    mosaik.util.connect_randomly(
        world, pv_models, meter_models, ('power_dc', 'Prod'), evenly=False)
    mosaik.util.connect_randomly(
        world, home_models, meter_models, ('Aggregate', 'Load'), evenly=False)


def main():
    ######################## IMPORT DATA ########################
    gdf_PVs, gdf_section = read_gdf_data()
    print('Read gdf_data')
    logger.info('Read gdf_data')
    # Houses from populationsim
    houses = read_house_data(gdf_section)
    print('Read house_data')
    logger.info('Read house_data')
    # Families from populationsim
    families = read_family_data(houses)
    print('Read family_data')
    logger.info('Read family_data')

    ######################## MOSAIK ENVIRONMENT ########################

    # World Definition
    world = mosaik.World(SIM_CONFIG, MK_CONFIG)
    # World Start
    meteoSim = world.start('csv', sim_start=START_DATE, datafile=METEO_PATH)
    homeSim = world.start('HouseholdSim', start_date=START_DATE)
    # pvSim = world.start('GISPVSIM', GisArea=gdf_PVs.to_dict(),
    pvSim = world.start('GISPVSIM', num_sec=NUM_SEC, start_date=START_DATE)
    meterSim = world.start(
        'MeterSim', start_date=START_DATE, collect_data=True)
    meteo = meteoSim.meteo()
    # World Create
    start_time = time.time()
    homeModels = homeSim.Home.create(num=len(families), people=families)
    time_houses = time.time() - start_time
    logger.debug(f'Homes created in {int(time_houses)} s')
    start_time = time.time()
    pvModels = pvSim.PVSystem.create(
        num=len(gdf_PVs), FIDS=gdf_PVs.FID.values.tolist(), PVSspec=PVSystemsSpec)
    time_PVs = time.time() - start_time
    logger.debug(f'PVs created in {int(time_PVs)} s')
    start_time = time.time()
    meterModels = meterSim.MeterSystem.create(num=len(gdf_section), duration=int(
        END/10/60/144), num_sec=NUM_SEC, ids=gdf_section.SEZ2011.values.tolist())
    time_meters = time.time() - start_time
    logger.debug(f'Meter created in {int(time_meters)} s')
    start_time = time.time()
    # World Connect
    connect_meters(world, pvModels, homeModels, meterModels)
    # Connect Meteo to PV
    for pv in pvModels:
        world.connect(meteo, pv, 'T_ext', 'zenith', 'k_b', 'k_d')
    time_connect = time.time() - start_time
    logger.debug(f'Connection made in {int(time_connect)} s')
    # World Run
    start_time = time.time()
    logger.info('Simulation started')
    world.run(until=END)
    time_sim = int(time.time() - start_time)
    print(f'Simulation took: {time_sim}')
    logger.info(f'Simulation took: {time_sim} s')
    attributes = {
        'num_sez': int(len(gdf_section)),
        'num_PVs': int(len(gdf_PVs)),
        'num_houses': int(len(houses)),
        'time_houses': round(time_houses, 3),
        'time_Pvs': round(time_PVs, 3),
        'time_meters': round(time_meters, 3),
        'time_connect': round(time_connect, 3),
        'time_sim': round(time_sim, 3),
    }

    Path("./OutputSimResults").mkdir(parents=True, exist_ok=True)
    with open('./OutputSimResults/output_attributes_%d_days_%d_sez.json' % (int(END/10/60/144), NUM_SEC), 'w') as outfile:
        json.dump(attributes, outfile, indent=4)
        pass


if __name__ == "__main__":
    main()
